/**
 * @file
 * TwentyTwenty.js.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.TwentyTwenty = {
    attach: function (context, settings) {
      $(".twentytwenty-image-field-container").twentytwenty({
        default_offset_pct: 0.5, // How much of the before image is visible when the page loads
        orientation: 'horizontal' // Orientation of the before and after images ('horizontal' or 'vertical')
      }).find('.twentytwenty-after-label').attr('data-before', 'aaaaaaaaa');
    }
  }
})(jQuery);